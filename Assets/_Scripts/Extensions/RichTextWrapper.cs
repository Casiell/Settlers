﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ExtensionMethods.RichText
{
	public static class RichTextWrapper
	{
		private static readonly Dictionary<RichTextTag, string> Tags = new Dictionary<RichTextTag, string>
		{
			{ RichTextTag.Align, "align" },
			{ RichTextTag.Color, "color" },
			{ RichTextTag.Alpha, "alpha" },
			{ RichTextTag.Bold, "b" },
			{ RichTextTag.Italic, "i" },
			{ RichTextTag.CharacterSpacing, "cspace" },
			{ RichTextTag.Font, "font" },
			{ RichTextTag.Indentation, "indent" },
			{ RichTextTag.LineHeight, "line-height" },
			{ RichTextTag.LineIndentation, "line-indent" },
			{ RichTextTag.TextLink, "link" },
			{ RichTextTag.Lowercase, "lowercase" },
			{ RichTextTag.Uppercase, "uppercase" },
			{ RichTextTag.Smallcaps, "smallcaps" },
			{ RichTextTag.Margin, "margin" },
			{ RichTextTag.MarginLeft, "margin-left" },
			{ RichTextTag.MarginRight, "margin-right" },
			{ RichTextTag.Mark, "mark" },
			{ RichTextTag.Monospacing, "mspace" },
			{ RichTextTag.Noparse, "noparse" },
			{ RichTextTag.NonBreakingSpaces, "nobr" },
			{ RichTextTag.PageBreak, "page" },
			{ RichTextTag.HorizontalPosition, "pos" },
			{ RichTextTag.FontSize, "size" },
			{ RichTextTag.HorizontalSpace, "space" },
			{ RichTextTag.InsertSprites, "sprite" },
			{ RichTextTag.Strikethrough, "s" },
			{ RichTextTag.Underline, "u" },
			{ RichTextTag.CustomStyle, "style" },
			{ RichTextTag.Subscript, "sub" },
			{ RichTextTag.Superscript, "sup" },
			{ RichTextTag.VerticalOffset, "voffset" },
			{ RichTextTag.TextWidth, "width" },
		};

		private static string GetTag(RichTextTag tag)
		{
			return Tags.TryGetValue(tag, out string ret) ? ret : "";
		}
		
		public enum RichTextTag
		{
			/// <summary>
			/// Set text horizontal alignment
			/// </summary>
			Align,
			/// <summary>
			/// Text color
			/// </summary>
			Color,
			/// <summary>
			/// Text alpha
			/// </summary>
			Alpha,
			/// <summary>
			/// Enable bold text
			/// </summary>
			Bold,
			/// <summary>
			/// Enable cursive text
			/// </summary>
			Italic,
			/// <summary>
			/// Set the distance between characters 
			/// </summary>
			CharacterSpacing,
			/// <summary>
			/// Set font
			/// </summary>
			Font,
			/// <summary>
			/// Set indentation
			/// </summary>
			Indentation,
			/// <summary>
			/// Set distance between lines
			/// </summary>
			LineHeight,
			/// <summary>
			/// Set indentation of each new line
			/// </summary>
			LineIndentation,
			/// <summary>
			/// Set hyperlink ID
			/// </summary>
			TextLink,
			/// <summary>
			/// Convert text to lowercase
			/// </summary>
			Lowercase,
			/// <summary>
			/// Convert text to uppercase
			/// </summary>
			Uppercase,
			/// <summary>
			/// Convert text to uppercase with smaller non-capital letters
			/// </summary>
			Smallcaps,
			/// <summary>
			/// Set text margin
			/// </summary>
			Margin,
			/// <summary>
			/// Set text left margin
			/// </summary>
			MarginLeft,
			/// <summary>
			/// Set text right margin
			/// </summary>
			MarginRight,
			/// <summary>
			/// Colored overlay on top of text
			/// </summary>
			Mark,
			/// <summary>
			/// Override font's spacing to turn it into monospace
			/// </summary>
			Monospacing,
			/// <summary>
			/// Disable rich text parsing
			/// </summary>
			Noparse,
			/// <summary>
			/// Disable breaking spaces
			/// </summary>
			NonBreakingSpaces,
			/// <summary>
			/// Add new page
			/// </summary>
			PageBreak,
			/// <summary>
			/// Set horizontal position of text
			/// </summary>
			HorizontalPosition,
			/// <summary>
			/// Font size
			/// </summary>
			FontSize,
			/// <summary>
			/// Add horizontal space
			/// </summary>
			HorizontalSpace,
			/// <summary>
			/// Insert sprites into text
			/// </summary>
			InsertSprites,
			/// <summary>
			/// Draw a line through the text
			/// </summary>
			Strikethrough,
			/// <summary>
			/// Draw a line under the text
			/// </summary>
			Underline,
			/// <summary>
			/// Use custom style
			/// </summary>
			CustomStyle,
			/// <summary>
			/// Small text on the bottom
			/// </summary>
			Subscript,
			/// <summary>
			/// Small text on the top
			/// </summary>
			Superscript,
			/// <summary>
			/// Set vertical offset
			/// </summary>
			VerticalOffset,
			/// <summary>
			/// Set horizontal size of text area
			/// </summary>
			TextWidth
		}

		public enum Alignment
		{
			Left,
			Right,
			Center
		}

		public enum Unit
		{
			Pixels,
			FontUnit,
			Percentage
		}

		private static string GetUnitString(Unit unit)
		{
			switch (unit)
			{
				case Unit.Pixels:
					return "px";
				case Unit.FontUnit:
					return "em";
				case Unit.Percentage:
					return "%";
				default:
					throw new ArgumentOutOfRangeException(nameof(unit), unit, null);
			}
		}

		private static string GetValueWithUnit(float value, Unit unit)
		{
			return $"{value}{GetUnitString(unit)}";
		}

		public static string SetAlignment(this string value, Alignment alignment)
		{
			switch (alignment)
			{
				case Alignment.Left:
					return value.SurroundWithTag(RichTextTag.Align, "left");
				case Alignment.Right:
					return value.SurroundWithTag(RichTextTag.Align, "right");
				case Alignment.Center:
					return value.SurroundWithTag(RichTextTag.Align, "center");
			}

			return value;
		}

		private static string ColorToHex(Color color)
		{
			return $"#{ColorUtility.ToHtmlStringRGB(color)}";
		}
		
		private static string ColorToHexWithAlpha(Color color)
		{
			return $"#{ColorUtility.ToHtmlStringRGBA(color)}";
		}

		#region Color

		public static string SetColor(this string value, Color color)
		{
			return value.SurroundWithTag(RichTextTag.Color, ColorToHex(color));
		}
		
		public static string SetColorWithAlpha(this string value, Color color)
		{
			return value.SurroundWithTag(RichTextTag.Color, ColorToHexWithAlpha(color));
		}

		public static string SetColor(this string value, string color)
		{
			return value.SurroundWithTag(RichTextTag.Color, color);
		}

		public static string SetAlpha(this string value, int alpha)
		{
			return value.SurroundWithTag(RichTextTag.Alpha, alpha.ToString("X"));
		}

		public static string SetAlpha(this string value, string alpha)
		{
			return value.SurroundWithTag(RichTextTag.Alpha, alpha);
		}
		
		public static string SetMark(this string value, Color color)
		{
			return value.SurroundWithTag(RichTextTag.Mark, ColorToHexWithAlpha(color));
		}

		public static string SetMark(this string value, string color)
		{
			return value.SurroundWithTag(RichTextTag.Mark, color);
		}

		#endregion
		
		public static string SetItalic(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Italic);
		}
		
		public static string SetBold(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Bold);
		}
		
		public static string SetStrikethrough(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Strikethrough);
		}
		
		public static string SetUnderline(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Underline);
		}
		
		public static string SetSuperscript(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Superscript);
		}
		
		public static string SetSubscript(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Subscript);
		}

		#region Spacing

		public static string SetCharacterSpacing(this string value, float spacing, Unit unit)
		{
			if (unit == Unit.Pixels)
			{
				throw new NotSupportedException($"{unit} is not supported for this method!");
			}
			return value.SurroundWithTag(RichTextTag.CharacterSpacing, GetValueWithUnit(spacing, unit), numericValue: true);
		}

		public static string SetCharacterSpacing(this string value, string spacing)
		{
			return value.SurroundWithTag(RichTextTag.CharacterSpacing, spacing, numericValue: true);
		}
		
		public static string SetMonospaceWidth(this string value, float width, Unit unit)
		{
			if (unit == Unit.Pixels)
			{
				throw new NotSupportedException($"{unit} is not supported for this method!");
			}
			return value.SurroundWithTag(RichTextTag.Monospacing, GetValueWithUnit(width, unit), numericValue: true);
		}
		
		public static string SetMonospaceWidth(this string value, string width)
		{
			return value.SurroundWithTag(RichTextTag.Monospacing, width, numericValue: true);
		}
		
		public static string SetNonBreakingSpaces(this string value)
		{
			return value.SurroundWithTag(RichTextTag.NonBreakingSpaces);
		}
		
		public static string GetSpaceTag(float width, Unit unit)
		{
			return GetSpaceTag(GetValueWithUnit(width, unit));
		}

		public static string GetSpaceTag(string width)
		{
			return string.Format(OpeningTagWithNumericValueFormat, GetTag(RichTextTag.HorizontalSpace), width);
		}
		
		public static string AddSpaceBefore(this string value, float width, Unit unit)
		{
			return $"{GetSpaceTag(width, unit)}{value}";
		}

		public static string AddSpaceBefore(this string value, string width)
		{
			return $"{GetSpaceTag(width)}{value}";
		}
		
		public static string AddSpaceAfter(this string value, float width, Unit unit)
		{
			return $"{value}{GetSpaceTag(width, unit)}";
		}

		public static string AddSpaceAfter(this string value, string width)
		{
			return $"{value}{GetSpaceTag(width)}";
		}

		#endregion

		#region Font

		public static string SetFont(this string value, Font font)
		{
			return value.SurroundWithTag(RichTextTag.Font, font.name);
		}

		public static string SetFont(this string value, string fontName)
		{
			return value.SurroundWithTag(RichTextTag.Font, fontName);
		}

		public static string SetFontSize(this string value, float size, Unit unit, string relative = "")
		{
			return value.SurroundWithTag(RichTextTag.FontSize, $"{relative}{GetValueWithUnit(size, unit)}", numericValue: true);
		}
		
		public static string SetFontSize(this string value, string size)
		{
			return value.SurroundWithTag(RichTextTag.FontSize, size, numericValue: true);
		}

		#endregion
		
		public static string SetIndentation(this string value, float indentation, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.Indentation, GetValueWithUnit(indentation, unit), numericValue: true);
		}

		public static string SetIndentation(this string value, string indentation)
		{
			return value.SurroundWithTag(RichTextTag.Indentation, indentation, numericValue: true);
		}

		public static string SetLink(this string value, string linkID)
		{
			return value.SurroundWithTag(RichTextTag.TextLink, linkID);
		}

		#region Capitalization

		public static string SetLowercase(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Lowercase);
		}
		
		public static string SetUppercase(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Uppercase);
		}
		
		public static string SetSmallcaps(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Smallcaps);
		}

		#endregion

		#region Margin

		public static string SetMargin(this string value, float margin, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.Margin, GetValueWithUnit(margin, unit), numericValue: true);
		}

		public static string SetMargin(this string value, string margin)
		{
			return value.SurroundWithTag(RichTextTag.Margin, margin, numericValue: true);
		}
		
		public static string SetMarginLeft(this string value, float margin, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.MarginLeft, GetValueWithUnit(margin, unit), numericValue: true);
		}

		public static string SetMarginLeft(this string value, string margin)
		{
			return value.SurroundWithTag(RichTextTag.MarginLeft, margin, numericValue: true);
		}
		
		public static string SetMarginRight(this string value, float margin, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.MarginRight, GetValueWithUnit(margin, unit), numericValue: true);
		}

		public static string SetMarginRight(this string value, string margin)
		{
			return value.SurroundWithTag(RichTextTag.MarginRight, margin, numericValue: true);
		}

		#endregion

		public static string DisableParsing(this string value)
		{
			return value.SurroundWithTag(RichTextTag.Noparse);
		}
		
		public static string InsertPageBreak(this string value)
		{
			return $"{GetTag(RichTextTag.PageBreak)}{value}";
		}
		
		public static string SetHorizontalPosition(this string value, float position, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.HorizontalPosition, GetValueWithUnit(position, unit), numericValue: true);
		}

		public static string SetHorizontalPosition(this string value, string position)
		{
			return value.SurroundWithTag(RichTextTag.HorizontalPosition, position, numericValue: true);
		}
		
		public static string SetVerticalOffset(this string value, float offset, Unit unit)
		{
			if (unit == Unit.Pixels)
			{
				throw new NotSupportedException($"{unit} is not supported for this method!");
			}
			return value.SurroundWithTag(RichTextTag.VerticalOffset, GetValueWithUnit(offset, unit), numericValue: true);
		}

		public static string SetVerticalOffset(this string value, string offset)
		{
			return value.SurroundWithTag(RichTextTag.VerticalOffset, offset, numericValue: true);
		}
		
		public static string SetTextWidth(this string value, float width, Unit unit)
		{
			return value.SurroundWithTag(RichTextTag.HorizontalPosition, GetValueWithUnit(width, unit), numericValue: true);
		}

		public static string SetTextWidth(this string value, string width)
		{
			return value.SurroundWithTag(RichTextTag.HorizontalPosition, width, numericValue: true);
		}

		#region Sprites

		public static string GetSpriteTag(string spriteName, Color color, string atlasName = "")
		{
			var builder = new StringBuilder(OpeningBracket);

			builder.Append(GetTag(RichTextTag.InsertSprites));

			if (!string.IsNullOrEmpty(atlasName))
			{
				builder.Append("=");
				builder.Append(atlasName);
			}

			builder.Append($" spriteName={spriteName}");

			builder.Append($" color={ColorToHexWithAlpha(color)}");

			builder.Append(ClosingBracket);

			return builder.ToString();
		}

		public static string GetSpriteTag(string spriteName, string atlasName = "", string color = "")
		{
			var builder = new StringBuilder(OpeningBracket);

			builder.Append(GetTag(RichTextTag.InsertSprites));

			if (!string.IsNullOrEmpty(atlasName))
			{
				builder.Append("=");
				builder.Append(atlasName);
			}

			builder.Append($" spriteName={spriteName}");

			if (!string.IsNullOrEmpty(color))
			{
				builder.Append($" color={color}");
			}

			builder.Append(ClosingBracket);

			return builder.ToString();
		}
		
		public static string GetSpriteTag(int spriteIndex, Color color, string atlasName = "")
		{
			var builder = new StringBuilder(OpeningBracket);

			builder.Append(GetTag(RichTextTag.InsertSprites));

			if (!string.IsNullOrEmpty(atlasName))
			{
				builder.Append("=");
				builder.Append(atlasName);
			}

			builder.Append($" index={spriteIndex}");

			builder.Append($" color={ColorToHexWithAlpha(color)}");

			builder.Append(ClosingBracket);

			return builder.ToString();
		}
		
		public static string GetSpriteTag(int spriteIndex, string atlasName = "", string color = "")
		{
			var builder = new StringBuilder(OpeningBracket);

			builder.Append(GetTag(RichTextTag.InsertSprites));

			if (!string.IsNullOrEmpty(atlasName))
			{
				builder.Append("=");
				builder.Append(atlasName);
			}

			builder.Append($" index={spriteIndex}");

			if (!string.IsNullOrEmpty(color))
			{
				builder.Append($" color={color}");
			}

			builder.Append(ClosingBracket);

			return builder.ToString();
		}

		#endregion
		
		public static string SetCustomStyle(this string value, string styleName)
		{
			return value.SurroundWithTag(RichTextTag.CustomStyle, styleName);
		}
		
		public static string SurroundWithTag(this string value, RichTextTag tag, string tagParameter = "", bool numericValue = false)
		{
			return value.SurroundWithTag(GetTag(tag), tagParameter, numericValue);
		}

		public static string SurroundWithTag(this string value, string tag, string tagParameter = "", bool numericValue = false)
		{
			if (string.IsNullOrWhiteSpace(tagParameter))
			{
				return $"{string.Format(OpeningTagFormat, tag)}{value}{GetClosingTag(tag)}";
			}

			if (numericValue)
			{
				return $"{string.Format(OpeningTagWithNumericValueFormat, tag, tagParameter)}{value}{GetClosingTag(tag)}";
			}
			
			return $"{string.Format(OpeningTagWithStringValueFormat, tag, tagParameter)}{value}{GetClosingTag(tag)}";
		}

		private static string GetClosingTag(string tag)
		{
			return string.Format(ClosingTagFormat, tag);
		}

		private const string OpeningBracket = "<";
		private const string ClosingBracket = ">";
		private const string ClosingTagFormat = OpeningBracket + "/{0}" + ClosingBracket;
		private const string OpeningTagFormat = OpeningBracket + "{0}" + ClosingBracket;
		private const string OpeningTagWithStringValueFormat = OpeningBracket + "{0}=\"{1}\"" + ClosingBracket;
		private const string OpeningTagWithNumericValueFormat = OpeningBracket + "{0}={1}" + ClosingBracket;
	}
}