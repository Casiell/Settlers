﻿using System;
using System.ComponentModel;

public static class EnumExtensions 
{
	public static T[] GetEnumValues<T>() where T : Enum
	{
		return (T[])Enum.GetValues(typeof(T));
	}
	
	public static string GetDescription(this Enum value)
	{
		var fi = value.GetType().GetField(value.ToString());
		var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

		return attributes.Length > 0 ? attributes[0].Description : value.ToString();
	}
}