﻿using UnityEngine;

namespace ExtensionMethods.Colors
{
	public static class ColorExtensions
	{
		public static Color WithAlpha(this Color color, float a)
		{
			return new Color(color.r, color.g, color.b, a);
		}

		public static Color WithR(this Color color, float r)
		{
			return new Color(r, color.g, color.b, color.a);
		}

		public static Color WithG(this Color color, float g)
		{
			return new Color(color.r, g, color.b, color.a);
		}

		public static Color WithB(this Color color, float b)
		{
			return new Color(color.r, color.g, b, color.a);
		}

		public static Color SetAlpha(this ref Color color, float a)
		{
			return color = new Color(color.r, color.g, color.b, a);
		}

		public static Color SetR(this ref Color color, float r)
		{
			return color = new Color(r, color.g, color.b, color.a);
		}

		public static Color SetG(this ref Color color, float g)
		{
			return color = new Color(color.r, g, color.b, color.a);
		}

		public static Color SetB(this ref Color color, float b)
		{
			return color = new Color(color.r, color.g, b, color.a);
		}
	}
}