﻿using System;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace ExtensionMethods.GameObjects
{
	public static class GameObjectExtensions
	{
		/// <summary>
		/// Resets position, rotation and scale to default values. In local space
		/// </summary>
		/// <param name="transform"></param>
		public static void ResetLocalValues(this Transform transform)
		{
			var vectorZero = Vector3.zero;
        
			transform.localPosition = vectorZero;
			transform.localEulerAngles = vectorZero;
			transform.localScale = Vector3.one;
		}
		
		/// <summary>
		/// Add component to game object if it isn't there already
		/// Return the component
		/// </summary>
		/// <param name="go"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T GetOrAddComponent<T>(this GameObject go) where T : Component
		{
			if (go.TryGetComponent<T>(out var component))
			{
				return component;
			}

			return go.AddComponent<T>();
		}
		
		public static T GetOrAddComponent<T>(this MonoBehaviour go) where T : Component
		{
			if (go.TryGetComponent<T>(out var component))
			{
				return component;
			}

			return go.gameObject.AddComponent<T>();
		}

#if UNITY_2019_2_OR_NEWER
		[Obsolete("Obsolete, use UnityEngine.GameObject.TryGetComponent instead")]
#endif
		public static bool TryGetComponent<T>(this GameObject go, out T component) where T : class
		{
#if UNITY_2019_2_OR_NEWER
			return go.TryGetComponent(out component);
#else
			component = go.GetComponent<T>();
			return !(component is null);
#endif
		}

#if UNITY_2019_2_OR_NEWER
		[Obsolete("Obsolete, use UnityEngine.GameObject.TryGetComponent instead")]
#endif
		public static bool TryGetComponent<T>(this MonoBehaviour go, out T component) where T : Component
		{
#if UNITY_2019_2_OR_NEWER
			return go.TryGetComponent(out component);
#else
			component = go.GetComponent<T>();
			return !(component is null);
#endif
		}

		public static void DestroyAllChildren(this Transform value)
		{
			int childs = value.childCount;
			for (int i = childs - 1; i >= 0; i--)
			{
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					Object.Destroy(value.GetChild(i).gameObject);
#if UNITY_EDITOR
				}
				else
				{
					Object.DestroyImmediate(value.GetChild(i).gameObject);
				}
#endif
			}
		}
	
		/// <summary>
		/// Checks if coroutine is null before stopping
		/// </summary>
		/// <param name="monoBehaviour"></param>
		/// <param name="coroutine"></param>
		public static void StopCoroutineSafe(this MonoBehaviour monoBehaviour, ref Coroutine coroutine)
		{
			if (coroutine != null)
			{
				monoBehaviour.StopCoroutine(coroutine);
				coroutine = null;
			}
		}

		[Conditional("UNITY_EDITOR")]
		public static void SetNameEditor(this Component go, string newName)
		{
			go.name = newName;
		}
		
		[Conditional("UNITY_EDITOR")]
		public static void SaveChanges(string message, GameObject go)
		{
#if UNITY_EDITOR
			UnityEditor.Undo.RegisterCompleteObjectUndo(go, message);
			UnityEditor.PrefabUtility.RecordPrefabInstancePropertyModifications(go);
#endif
		}
	
		[Conditional("UNITY_EDITOR")]
		public static void SaveChanges(string message, Component component)
		{
#if UNITY_EDITOR
			UnityEditor.Undo.RegisterCompleteObjectUndo(component, message);
			UnityEditor.PrefabUtility.RecordPrefabInstancePropertyModifications(component);
#endif
		}
 
		public static void CenterOnItem(this ScrollRect scrollRect, RectTransform target)
		{
			var scrollRectTransform = (RectTransform)scrollRect.transform;
			var content = scrollRect.content;
			var viewport = scrollRect.viewport;
		
			// Item is here
			var itemCenterPositionInScroll = GetWorldPointInWidget(scrollRectTransform, GetWidgetWorldPoint(target));
			// But must be here
			var targetPositionInScroll = GetWorldPointInWidget(scrollRectTransform, GetWidgetWorldPoint(viewport));
			// So it has to move this distance
			var difference = targetPositionInScroll - itemCenterPositionInScroll;
			difference.z = 0f;
 
			//clear axis data that is not enabled in the scrollrect
			if (!scrollRect.horizontal)
			{
				difference.x = 0f;
			}
			if (!scrollRect.vertical)
			{
				difference.y = 0f;
			}

			var rect = scrollRectTransform.rect;

			var contentRect = content.rect;

			float differenceX = difference.x / (contentRect.size.x - rect.size.x);
			float differenceY = difference.y / (contentRect.size.y - rect.size.y);
			var normalizedDifference = new Vector2(differenceX, differenceY);
 
			var newNormalizedPosition = scrollRect.normalizedPosition - normalizedDifference;
			if (scrollRect.movementType != ScrollRect.MovementType.Unrestricted)
			{
				newNormalizedPosition.x = Mathf.Clamp01(newNormalizedPosition.x);
				newNormalizedPosition.y = Mathf.Clamp01(newNormalizedPosition.y);
			}
 
			scrollRect.normalizedPosition = newNormalizedPosition;
		}
		private static Vector3 GetWidgetWorldPoint(RectTransform target)
		{
			//pivot position + item size has to be included
			var pivotOffset = new Vector3((0.5f - target.pivot.x) * target.rect.size.x,
				(0.5f - target.pivot.y) * target.rect.size.y,
				0f);
			var localPosition = target.localPosition + pivotOffset;
			return target.parent.TransformPoint(localPosition);
		}
		private static Vector3 GetWorldPointInWidget(RectTransform target, Vector3 worldPoint)
		{
			return target.InverseTransformPoint(worldPoint);
		}
	}
}