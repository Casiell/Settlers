﻿using System.Collections.Generic;
using System.Linq;
using ExtensionMethods.Rects;
using UnityEngine;

namespace ExtensionMethods.Vectors
{
	public static class VectorExtensions
	{
		#region Vector3

		public static Vector3 WithX(this Vector3 value, float x)
		{
			return new Vector3(x, value.y, value.z);
		}

		public static Vector3 WithY(this Vector3 value, float y)
		{
			return new Vector3(value.x, y, value.z);
		}

		public static Vector3 WithZ(this Vector3 value, float z)
		{
			return new Vector3(value.x, value.y, z);
		}

		public static Vector3 Absolute(this Vector3 value)
		{
			return new Vector3(Mathf.Abs(value.x), Mathf.Abs(value.y), Mathf.Abs(value.z));
		}

		public static Vector3 SetX(this ref Vector3 value, float x)
		{
			value.Set(x, value.y, value.z);
			return value;
		}

		public static Vector3 SetY(this ref Vector3 value, float y)
		{
			value.Set(value.x, y, value.z);
			return value;
		}

		public static Vector3 SetZ(this ref Vector3 value, float z)
		{
			value.Set(value.x, value.y, z);
			return value;
		}

		public static Vector3 SetAbsolute(this ref Vector3 value)
		{
			value.Set(Mathf.Abs(value.x), Mathf.Abs(value.y), Mathf.Abs(value.z));
			return value;
		}
		
		public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
		{
			var ab = b - a;
			var av = value - a;
			return Vector3.Dot(av, ab) / Vector3.Dot(ab, ab);
		}
		
		public static Vector3 Average(this IEnumerable<Vector3> value)
		{
			return new Vector3(value.Average(vector => vector.x), value.Average(vector => vector.y), value.Average(vector => vector.z));
		}
		
		public static float SquaredDistance(Vector3 a, Vector3 b)
		{
			return Vector3.SqrMagnitude(a - b);
		}

		public static Vector3 DivideBy(this Vector3 a, Vector3 b)
		{
			return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
		}
		
		public static Vector3 MultipltBy(this Vector3 a, Vector3 b)
		{
			return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
		}
		
		public static Bounds GetBounds(this IEnumerable<Vector3> points)
		{
			if (!points.Any())
			{
				Debug.LogWarning($"{nameof(VectorExtensions)}::{nameof(GetBounds)} - No points, returning default bounds!");
				return new Bounds();
			}

			var bounds = new Bounds { center = points.First() };
			foreach (var point in points)
			{
				bounds.Encapsulate(point);
			}

			return bounds;
		}

		#endregion Vector3

		#region Vector2

		public static Vector2 WithX(this Vector2 value, float x)
		{
			return new Vector2(x, value.y);
		}

		public static Vector2 WithY(this Vector2 value, float y)
		{
			return new Vector2(value.x, y);
		}

		public static Vector2 SetX(this ref Vector2 value, float x)
		{
			value.Set(x, value.y);
			return value;
		}

		public static Vector2 SetY(this ref Vector2 value, float y)
		{
			value.Set(value.x, y);
			return value;
		}

		public static float SquaredDistance(Vector2 a, Vector2 b)
		{
			return Vector2.SqrMagnitude(a - b);
		}

		/// <summary>
		/// Converts Vector2 to Vector3 and sets it's z value.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Vector3 WithZ(this Vector2 value, float z)
		{
			return new Vector3(value.x, value.y, z);
		}

		public static Vector2 Absolute(this Vector2 value)
		{
			return new Vector2(Mathf.Abs(value.x), Mathf.Abs(value.y));
		}

		public static Vector2 SetAbsolute(this ref Vector2 value)
		{
			value.Set(Mathf.Abs(value.x), Mathf.Abs(value.y));
			return value;
		}
		public static Vector2 Average(this IEnumerable<Vector2> value)
		{
			return new Vector2(value.Average(vector => vector.x), value.Average(vector => vector.y));
		}
		
		public static Vector2 Average(params Vector2[] value)
		{
			return new Vector2(value.Average(vector => vector.x), value.Average(vector => vector.y));
		}
		
		public static Vector2 DivideBy(this Vector2 a, Vector2 b)
		{
			return new Vector3(a.x / b.x, a.y / b.y);
		}
		
		public static Bounds GetBounds(this IEnumerable<Vector2> points)
		{
			if (!points.Any())
			{
				Debug.LogWarning($"{nameof(VectorExtensions)}::{nameof(GetBounds)} - No points, returning default bounds!");
				return new Bounds();
			}

			var bounds = new Bounds { center = points.First() };
			foreach (var point in points)
			{
				bounds.Encapsulate(point);
			}

			return bounds;
		}
		
		public static Rect GetRect(this IEnumerable<Vector2> points)
		{
			if (!points.Any())
			{
				Debug.LogWarning($"{nameof(VectorExtensions)}::{nameof(GetRect)} - No points, returning default rect!");
				return new Rect();
			}

			var bounds = new Rect { center = points.First() };
			foreach (var point in points)
			{
				bounds.Encapsulate(point);
			}

			return bounds;
		}
		
		/// <summary>
		/// Return central point of a zone described by vertices
		/// </summary>
		/// <param name="zone"></param>
		/// <returns></returns>
		public static Vector2 CalculateZoneCenter(IEnumerable<Vector2> zone)
		{
			var center = Vector2.zero;

			if (!zone.Any())
			{
				return center;
			}

			return zone.Aggregate(center, (current, point) => current + point) / zone.Count();
		}

		#endregion Vector2

		public static Vector3 ToVector3(this Vector2 value)
		{
			return value;
		}

		public static Vector2 ToVector2(this Vector3 value)
		{
			return value;
		}
	}
}