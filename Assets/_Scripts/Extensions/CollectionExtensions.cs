﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ExtensionMethods.Collections
{
	public static class CollectionExtensions
	{
		/// <summary>
		/// Add element to dictionary if there is none with the same key
		/// </summary>
		/// <param name="dictionary"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <typeparam name="K"></typeparam>
		/// <typeparam name="V"></typeparam>
		/// <returns>Returns true if the element was added. False if the element was already in the dictionary</returns>
		public static bool AddIfNotContains<K, V>(this IDictionary<K, V> dictionary, K key, V value)
		{
			if (!dictionary.ContainsKey(key))
			{
				dictionary.Add(key, value);
				return true;
			}

			return false;
		}

		public static bool AddIfNotContains<T>(this IList<T> collection, T value)
		{
			if (collection.Contains(value))
			{
				return false;
			}
			
			collection.Add(value);
			return true;
		}

		/// <summary>
		/// Add element to dictionary, or update it's value
		/// </summary>
		/// <param name="dictionary"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <typeparam name="K"></typeparam>
		/// <typeparam name="V"></typeparam>
		public static void AddOrUpdate<K, V>(this IDictionary<K, V> dictionary, K key, V value)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key] = value;
			}
			else
			{
				dictionary.Add(key, value);
			}
		}
		
		/// <summary>
		/// Wraps this object instance into an IEnumerable&lt;T&gt;
		/// consisting of a single item.
		/// </summary>
		/// <typeparam name="T"> Type of the object. </typeparam>
		/// <param name="item"> The instance that will be wrapped. </param>
		/// <returns> An IEnumerable&lt;T&gt; consisting of a single item. </returns>
		public static IEnumerable<T> Yield<T>(this T item)
		{
			yield return item;
		}

		/// <summary>
		/// If element exist, sum values
		/// </summary>
		/// <param name="dict"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <typeparam name="T"></typeparam>
		public static void AddOrIncrement<T>(this Dictionary<T, float> dict, T key, float value)
		{
			if (dict.ContainsKey(key))
			{
				dict[key] += value;
				return;
			}
			
			dict.Add(key, value);
		}

		public static void AddOrChange<K, V>(this Dictionary<K, V> dict, K key, V value, Func<V, V, V> transformationFunc)
		{
			if (dict.TryGetValue(key, out var current))
			{
				dict[key] = transformationFunc(current, value);
				return;
			}
			
			dict.Add(key, value);
		}
		
		/// <summary>
		/// Returns elements up to and including first element that satisfies the predicate
		/// </summary>
		/// <param name="data"></param>
		/// <param name="predicate"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static IEnumerable<T> TakeUntil<T>(this IEnumerable<T> data, Func<T, bool> predicate) 
		{
			foreach (var item in data) 
			{
				yield return item;
				
				if (predicate(item))
				{
					break;
				}
			}
		}
		
		public static T Random<T>(this IEnumerable<T> This)
		{
			if (!This.Any())
				return default;

			if (This.Count() == 1)
				return This.ElementAt(0);

			return This.ElementAt(UnityEngine.Random.Range(0, This.Count()));
		}
		
		public static T Random<T>(this IEnumerable<T> This, out int index)
		{
			index = -1;
			if (!This.Any())
				return default;

			if (This.Count() == 1)
			{
				index = 0;
				return This.ElementAt(0);
			}

			index = UnityEngine.Random.Range(0, This.Count());
			return This.ElementAt(index);
		}

		/// <summary>
		/// Get multiple unique random elements
		/// </summary>
		/// <param name="list"></param>
		/// <param name="elementCount"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static IEnumerable<T> Random<T>(this IList<T> list, int elementCount)
		{
			if (elementCount >= list.Count)
			{
				return list;
			}
			
			var randomIndices = new HashSet<int>();

			while (randomIndices.Count < elementCount)
			{
				int random = UnityEngine.Random.Range(0, list.Count);
				randomIndices.Add(random);
			}

			return randomIndices.Select(x => list[x]);
		}
	}
}