﻿using UnityEngine;
using UnityEngine.UI;

namespace ExtensionMethods.Rects
{
	public static class RectExtensions
	{
		public static Rect Encapsulate(this Rect rect, Vector2 position)
		{
			rect.xMin = Mathf.Min(rect.xMin, position.x);
			rect.xMax = Mathf.Min(rect.xMax, position.x);

			rect.yMin = Mathf.Min(rect.yMin, position.y);
			rect.yMax = Mathf.Max(rect.yMax, position.y);
			
			return rect;
		}

		public static Rect Encapsulate(this Rect rect, Rect otherRect)
		{
			rect = rect.Encapsulate(otherRect.min);
			rect = rect.Encapsulate(otherRect.max);
			return rect;
		}

		public static Rect GetCommonBounds(this RectTransform rectTransform, bool scale)
		{
			var result = rectTransform.rect;
			result.center = rectTransform.position;
			if (!rectTransform.GetComponent<MaskableGraphic>())
			{
				result.size = Vector2.zero;
			}

			foreach (Transform child in rectTransform.transform)
			{
				if (child.GetComponent<IgnoreRectBounds>())
				{
					continue;
				}

				if (!child.TryGetComponent<RectTransform>(out var childRectTransform))
				{
					continue;
				}
				var childBounds = childRectTransform.GetCommonBounds(scale);

				if (Mathf.Approximately(result.size.sqrMagnitude, 0))
				{
					result = childBounds;
				}
				else
				{
					result = result.Encapsulate(childBounds);
				}
			}
        
			if (scale)
			{
				var center = result.center;
				result.size *= rectTransform.localScale;
				result.center = center;
			}

			return result;
		}
	}
	
	public class IgnoreRectBounds : MonoBehaviour
	{ }
}