﻿using System;
using UnityEngine;

namespace ExtensionMethods
{
	/// <summary>
	/// Extension method for types that are not common enough to warrant their own class
	/// </summary>
	public static class GeneralExtensionMethods
	{
		/// <summary>
		/// Replaces common formatting mistakes that prevent the text from displaying correctly
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string FixFormattingMistakes(this string value)
		{
			return value
				.ReplaceNewlineCharacter()
				.Replace("<align=Left>", "<align=left>");
		}

		/// <summary>
		/// Replaces \r with \n
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string ReplaceNewlineCharacter(this string value)
		{
			return value.Replace("\r", "\n");
		}
		
		public static bool IsNone(this string value)
		{
			return string.IsNullOrEmpty(value) || value == "None";
		}

		public static T[] GetEnumValues<T>() where T : Enum
		{
			return (T[])Enum.GetValues(typeof(T));
		}
		
		public static string ToEnglishNumber(this int number)
		{
			bool isTenNumber = number % 100 > 10 && number % 100 < 20;

			switch (number % 10)
			{
				case 1 when !isTenNumber:
					return $"{number.ToString()}st";
				case 2 when !isTenNumber:
					return $"{number.ToString()}nd";
				case 3 when !isTenNumber:
					return $"{number.ToString()}rd";
				default:
					return $"{number.ToString()}th";
			}
		}

		public static bool RaycastFromCursor(this Camera camera, LayerMask layerMask, float maxDistance, out RaycastHit hit)
		{
			var ray = camera.ScreenPointToRay(Input.mousePosition);
			return Physics.Raycast(ray, out hit, maxDistance, layerMask, QueryTriggerInteraction.Ignore);
		}
	}
}