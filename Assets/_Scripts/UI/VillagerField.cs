using TMPro;
using UnityEngine;

public class VillagerField : MonoBehaviour
{
    [SerializeField] private TMP_Text textfield;
    public void UpdateView(int currentVilllagersSpawned, int maxVillagers)
    {
        textfield.SetText($"{currentVilllagersSpawned.ToString()}/{maxVillagers.ToString()}");
    }
}
