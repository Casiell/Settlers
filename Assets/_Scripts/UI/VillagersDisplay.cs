using UnityEngine;

public class VillagersDisplay : MonoBehaviour
{
    [SerializeField] private VillagerManager manager;
    [SerializeField] private VillagerField field;

    private void Start()
    {
        manager.VillagerCountChanged += OnVillagerCountChanged;
        field.UpdateView(manager.CurrentSpawnedVillagers, manager.MaxVillagers);
    }

    private void OnVillagerCountChanged(VillagerManager.VillagerCountChangedParams @params)
    {
        field.UpdateView(@params.CurrentlySpawnedVillagers, @params.MaxVillagers);
    }
}
