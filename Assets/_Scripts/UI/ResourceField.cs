using TMPro;
using UnityEngine;

public class ResourceField : MonoBehaviour
{
    [SerializeField] private TMP_Text textField;
    
    public void UpdateView(ResourceManager.ResourceChangedParameters resourceChangedParameters)
    {
        UpdateView(resourceChangedParameters.NewCount);
    }
    
    public void UpdateView(int count)
    {
        textField.SetText(count.ToString());
    }
}
