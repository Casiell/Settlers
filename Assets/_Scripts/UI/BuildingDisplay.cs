using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class BuildingDisplay : SerializedMonoBehaviour
{
    [OdinSerialize, SceneObjectsOnly] private Transform parent;
    [OdinSerialize, AssetsOnly, AssetSelector] private BuildingButton buttonPrefab;

    private void Start()
    {
        foreach (var buildingData in BuildingSpawner.Instance.BuildingDatas)
        {
            var button = Instantiate(buttonPrefab, parent);
            button.Init(buildingData);
        }
    }
}
