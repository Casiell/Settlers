using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private Image icon;

    private BuildingData data;

    private void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    private void OnResourceCountChanged(ResourceManager.ResourceChangedParameters obj)
    {
        SetInteractable();
    }

    private void SetInteractable()
    {
        button.interactable = ResourceManager.Instance.HasEnoughResources(data.Cost);
    }

    public void Init(BuildingData buildingData)
    {
        name = buildingData.name;
        icon.sprite = buildingData.Icon;
        data = buildingData;
        
        ResourceManager.Instance.ResourceCountChanged += OnResourceCountChanged;
        SetInteractable();
    }

    private void OnClick()
    {
        BuildingSpawner.Instance.StartBuildingPlacement(data);
    }
}
