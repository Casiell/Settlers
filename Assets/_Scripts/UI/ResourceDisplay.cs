using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class ResourceDisplay : SerializedMonoBehaviour
{
    [OdinSerialize] private ResourceManager resourceManager;
    [OdinSerialize] private Dictionary<ResourceType, ResourceField> resourceFieldMap = new();

    private void Start()
    {
        resourceManager.ResourceCountChanged += OnResourceCountChanged;

        foreach (var (resourceType, resourceField) in resourceFieldMap)
        {
            resourceField.UpdateView(resourceManager.GetResourceCount(resourceType));
        }
    }

    private void OnResourceCountChanged(ResourceManager.ResourceChangedParameters resourceChangedParameters)
    {
        if (resourceFieldMap.TryGetValue(resourceChangedParameters.Type, out var field))
        {
            field.UpdateView(resourceChangedParameters);
        }
    }
}
