using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/BuildingData", fileName = "BuildingData", order = 0)]
public class BuildingData : SerializedScriptableObject
{
    [Title("Misc")]
    [OdinSerialize] public string Name { get; private set; }
    [OdinSerialize, AssetsOnly] public Sprite Icon { get; private set; }
    
    [Title("Pre spawn data")]
    [OdinSerialize] public Dictionary<ResourceType, int> Cost { get; private set; } = new();
    [OdinSerialize, AssetsOnly, AssetSelector] public Building Prefab { get; private set; }
    [OdinSerialize, AssetsOnly] public GameObject ViewOnlyPrefab { get; private set; }

    [Title("Functional data")]
    [OdinSerialize] public int VillagerCount { get; private set; }
    [OdinSerialize] public Profession WorkerProfession { get; private set; }
    
    [OdinSerialize] public ResourceType ProducedResource { get; private set; }
    [OdinSerialize] public int Range { get; private set; }
    public int RangeSquared => Range * Range;
    [OdinSerialize] public int MaxStorage { get; private set; }
}
