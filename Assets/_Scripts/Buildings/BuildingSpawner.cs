using System;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;
using ExtensionMethods.Vectors;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class BuildingSpawner : SerializedMonoBehaviour
{
    public static BuildingSpawner Instance { get; private set; }

    [OdinSerialize] private LayerMask terrainLayer;
    [OdinSerialize] public List<BuildingData> BuildingDatas { get; private set; }

    [OdinSerialize] private float rotationSpeed = 5;
    [OdinSerialize] private GameObject buildingRangeDisplayPrefab;

    private GameObject currentlyPlacedBuilding;
    private BuildingData currentlyPlacedBuildingData;
    private float currentHeight = 0;

    [OdinSerialize] private Dictionary<Type, List<Building>> allBuildings = new();

    private void Awake()
    {
        Instance = this;
    }

    public void StartBuildingPlacement(BuildingData data)
    {
        if (!data)
        {
            Debug.LogError("Attempted to start building placement, but no data was provided");
            return;
        }

        if (!ResourceManager.Instance.HasEnoughResources(data.Cost))
        {
            return;
        }
        var position = Vector3.zero;
        
        if (Camera.main.RaycastFromCursor(terrainLayer, 1000, out var hit))
        {
            position = hit.point;
        }

        currentlyPlacedBuilding = Instantiate(data.ViewOnlyPrefab, position, Quaternion.identity);
        SpawnRangeIdicator(data, currentlyPlacedBuilding.transform);
        currentlyPlacedBuildingData = data;
    }

    private void SpawnRangeIdicator(BuildingData data, Transform buildingDummy)
    {
        var rangeIndicator = Instantiate(buildingRangeDisplayPrefab, buildingDummy);
        var baseScale = buildingRangeDisplayPrefab.transform.localScale;
        rangeIndicator.transform.localScale = baseScale * data.Range;
    }

    private void Update()
    {
        if (!currentlyPlacedBuilding)
        {
            return;
        }

        if (Camera.main.RaycastFromCursor(terrainLayer, 1000, out var hit))
        {
            var pointY = hit.point.y;
            currentlyPlacedBuilding.transform.position = hit.point.WithY(pointY + currentHeight);

            if (Input.GetMouseButtonDown(0))
            {
                SpawnBuilding(currentlyPlacedBuildingData, hit.point, currentlyPlacedBuilding.transform.rotation);
                ClearCurrentPlacing();
            }
            else
            {
                if (Input.GetKey(KeyCode.E))
                {
                    currentlyPlacedBuilding.transform.Rotate(currentlyPlacedBuilding.transform.up, rotationSpeed);
                }
                else if (Input.GetKey(KeyCode.Q))
                {
                    currentlyPlacedBuilding.transform.Rotate(currentlyPlacedBuilding.transform.up, -rotationSpeed);
                }

                if (Input.GetMouseButtonDown(2))
                {
                    currentHeight = 0;
                }
                else
                {
                    currentHeight = Mathf.Max(currentHeight + Input.mouseScrollDelta.y, 0);
                }
            }
        }
    }

    private void SpawnBuilding(BuildingData data, Vector3 position, Quaternion rotation)
    {
        if (ResourceManager.Instance.TryRemoveResource(data.Cost))
        {
            var building = Instantiate(currentlyPlacedBuildingData.Prefab, position, rotation);
            building.OnPlaced(data);
            var type = building.GetType();

            if (allBuildings.TryGetValue(type, out var list))
            {
                list.Add(building);
            }
            else
            {
                allBuildings.Add(type, new List<Building> { building });
            }
        }
    }

    public bool GetClosestBuilding<T>(Vector3 position, out T closestBuilding) where T : Building
    {
        closestBuilding = null;
        if (allBuildings.TryGetValue(typeof(T), out var list) && list.Any())
        {
            Building tempClosestBuilding = null;
            var closestDistance = float.MaxValue;

            foreach (var building in list)
            {
                var distance = VectorExtensions.SquaredDistance(building.EntrancePoint.position, position);

                if (distance < closestDistance)
                {
                    tempClosestBuilding = building;
                    closestDistance = distance;
                }
            }

            closestBuilding = tempClosestBuilding as T;
            return true;
        }

        return false;
    }

    private void ClearCurrentPlacing()
    {
        if (currentlyPlacedBuilding)
        {
            Destroy(currentlyPlacedBuilding);
        }

        currentlyPlacedBuilding = null;
        currentlyPlacedBuildingData = null;
    }
}
