public class Storage : Building
{
    public void AddResource(ResourceType resourceType, int count)
    {
        ResourceManager.Instance.AddResource(resourceType, count);
    }
}
