using System.Collections.Generic;
using System.Linq;
using ExtensionMethods.Vectors;
using UnityEngine;

public class ResourceGatheringBuilding : Building
{
	private readonly List<IObjectGivingResource> treesInRange = new();

	private int internalStorage;

	public override void OnPlaced(BuildingData data)
	{
		base.OnPlaced(data);
		
		FindTreesInRange();
	}

	private void Update()
	{
		if (treesInRange.Any() && internalStorage < buildingData.MaxStorage)
		{
			foreach (var worker in workers)
			{
				if (worker.IsIdle)
				{
					var resourceSite = treesInRange.First();
					worker.GatherResource(resourceSite, EntrancePoint, () => OnGatheringFinished(worker.RemoveResource()));
					treesInRange.Remove(resourceSite);
				}
			}
		}
	}

	private void OnGatheringFinished(ResourceType resourceType)
	{
		if (resourceType != buildingData.ProducedResource)
		{
			Debug.LogWarning($"Attempted to store {resourceType.ToString()} in a building producing {buildingData.ProducedResource.ToString()}");
			return;
		}
		++internalStorage;

		if (internalStorage >= buildingData.MaxStorage)
		{
			ScheduleCarrierToPickUp();
		}
	}

	private void ScheduleCarrierToPickUp()
	{
		var entrancePosition = EntrancePoint.position;

		if (!BuildingSpawner.Instance.GetClosestBuilding(entrancePosition, out Storage storage))
		{
			return;
		}

		if (!VillagerManager.Instance.TryRequestNewVillager(out var villager, Profession.Carrier))
		{
			return;
		}

		villager.GoTo(entrancePosition, () =>
		{
			var resourcesHeld = internalStorage;
			internalStorage = 0;

			villager.GoTo(storage.EntrancePoint.position, () =>
			{
				storage.AddResource(buildingData.ProducedResource, resourcesHeld);
				VillagerManager.Instance.ReturnVillager(villager);
			});
		});
	}

	private void FindTreesInRange()
	{
		treesInRange.Clear();
		var allTrees = FindObjectsOfType<IObjectGivingResource>()
			.Where(x => VectorExtensions.SquaredDistance(x.transform.position, transform.position) < buildingData.RangeSquared && x.ResourceGiven == buildingData.ProducedResource);
		
		foreach (var tree in allTrees)
		{
			treesInRange.Add(tree);
			tree.Highlight(true);
		}
	}
	
	private void OnDrawGizmosSelected()
	{
		if (buildingData)
		{
			Gizmos.DrawWireSphere(transform.position, buildingData.Range);
		}
	}
}
