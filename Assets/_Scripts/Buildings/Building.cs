using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public abstract class Building : SerializedMonoBehaviour
{
    [OdinSerialize, Required] public Transform EntrancePoint { get; private set; }
    [OdinSerialize, ReadOnly] protected List<Villager> workers = new();
    protected BuildingData buildingData;
    
    public virtual void OnPlaced(BuildingData data)
    {
        buildingData = data;
        for (int i = 0; i < buildingData.VillagerCount; i++)
        {
            if (VillagerManager.Instance.TryRequestNewVillager(out var villager))
            {
                villager.GoTo(EntrancePoint.position, () =>
                {
                    workers.Add(VillagerManager.Instance.Advance(villager, buildingData.WorkerProfession));
                });
            }
        }
    }
}
