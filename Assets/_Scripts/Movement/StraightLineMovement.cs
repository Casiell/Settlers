using System;
using DG.Tweening;
using UnityEngine;

public class StraightLineMovement : MovementBase
{
	public override void GoTo(Vector3 targetPosition, float speed, Action onMovementFinished = null)
	{
		var position = transform.position;
		var distance = Vector3.Distance(position, targetPosition);
		Transform thisTransform;
		var move = (thisTransform = transform).DOMove(targetPosition, distance / speed);
		thisTransform.forward = (targetPosition - position).normalized;
		move.onComplete += () =>
		{
			onMovementFinished?.Invoke();
		};
	}
}
