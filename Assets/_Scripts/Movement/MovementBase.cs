using System;
using UnityEngine;

public abstract class MovementBase : MonoBehaviour
{
    public abstract void GoTo(Vector3 targetPosition, float speed, Action onMovementFinished = null);
}
