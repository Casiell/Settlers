using ExtensionMethods.Vectors;
using UnityEngine;

public class GroundSnapper : MonoBehaviour
{
    [SerializeField] private LayerMask TerrainLayerMask;
    private Transform transformToSnap;

    private void Start()
    {
        transformToSnap = transform;
    }

    private void LateUpdate()
    {
        var currentPosition = transformToSnap.position;

        if (Physics.Raycast(currentPosition + Vector3.up * 10, Vector3.down, out var hit, 100, TerrainLayerMask, QueryTriggerInteraction.Ignore))
        {
            transformToSnap.position = currentPosition.WithY(hit.point.y);
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} out of bounds!");
        }
    }
}
