using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class VillagerManager : SerializedMonoBehaviour
{
    public event Action<VillagerCountChangedParams> VillagerCountChanged;
    
    public static VillagerManager Instance { get; private set; }
    [OdinSerialize] public int MaxVillagers { get; private set; }
    public int CurrentSpawnedVillagers { get; private set; }
    [OdinSerialize, SceneObjectsOnly] private Transform spawnPoint;
    [OdinSerialize, AssetsOnly] private Dictionary<Profession, Villager> prefabDictionary;

    private void Awake()
    {
        Instance = this;
    }

    public bool TryRequestNewVillager(out Villager villager)
    {
        if (CurrentSpawnedVillagers < MaxVillagers)
        {
            villager = SpawnNewVillager(Profession.Villager);
            return true;
        }

        villager = null;
        return false;
    }

    public bool TryRequestNewVillager(out Villager villager, Profession profession)
    {
        if (CurrentSpawnedVillagers < MaxVillagers)
        {
            villager = SpawnNewVillager(profession);
            return true;
        }

        villager = null;
        return false;
    }

    private Villager SpawnNewVillager(Profession profession)
    {
        ++CurrentSpawnedVillagers; 
        var villager = Spawn(profession, spawnPoint.position, spawnPoint.rotation);
        VillagerCountChanged?.Invoke(new VillagerCountChangedParams(CurrentSpawnedVillagers, MaxVillagers));
        return villager;
    }

    public void ReturnVillager(Villager villager)
    {
        villager.GoTo(spawnPoint.position, () => ReturnVillagerInternal(villager));
    }

    private void ReturnVillagerInternal(Villager villager)
    {
        --CurrentSpawnedVillagers;
        Despawn(villager);
        VillagerCountChanged?.Invoke(new VillagerCountChangedParams(CurrentSpawnedVillagers, MaxVillagers));
    }

    public Villager Advance(Villager villager, Profession profession)
    {
        var villagerTransform = villager.transform;
        var position = villagerTransform.position;
        var rotation = villagerTransform.rotation;
        Despawn(villager);
        var advanced = Spawn(profession, position, rotation);
        return advanced;
    }

    private Villager Spawn(Profession profession, Vector3 spawnPosition, Quaternion spawnRotation)
    {
        return Instantiate(prefabDictionary[profession], spawnPosition, spawnRotation);
    }

    private void Despawn(Villager villager)
    {
        Destroy(villager.gameObject);
    }

    public readonly struct VillagerCountChangedParams
    {
        public readonly int CurrentlySpawnedVillagers;
        public readonly int MaxVillagers;

        public VillagerCountChangedParams(int currentlySpawnedVillagers, int maxVillagers)
        {
            CurrentlySpawnedVillagers = currentlySpawnedVillagers;
            MaxVillagers = maxVillagers;
        }
    }
}