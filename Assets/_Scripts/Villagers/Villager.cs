using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class Villager : SerializedMonoBehaviour
{
    [Title("Components")] 
    [OdinSerialize] private MovementBase movementComponent;

    [Title("Properties")]
    [OdinSerialize] public int Speed { get; private set; } = 2;

    [OdinSerialize, ReadOnly] private State currentState;

    [OdinSerialize] private Transform toolSlot;
    [OdinSerialize] private Transform resourceSlot;

    public bool IsIdle => currentState == State.Idle;

    private ResourceType resourceHeld = ResourceType.None;

    public void GoTo(Vector3 targetPosition, Action onMovementFinished = null)
    {
        currentState = State.Walking;
        movementComponent.GoTo(targetPosition, Speed, () =>
        {
            currentState = State.Idle;
            onMovementFinished?.Invoke();
        });
    }

    public void GoThereAndBack(Vector3 targetPosition, Vector3 returnPosition, Action onTargetReached = null, Action onReturned = null)
    {
        currentState = State.Walking;
        movementComponent.GoTo(targetPosition, Speed, () =>
        {
            onTargetReached?.Invoke();

            movementComponent.GoTo(targetPosition, Speed, () =>
            {
                onReturned?.Invoke();
                currentState = State.Idle;
            });
        });
    }

    public void GatherResource(IObjectGivingResource target, Transform storage, Action gatheringFinished = null)
    {
        currentState = State.Gathering;
        
        var interactionTime = target.InteractionTime;
        var storagePosition = storage.transform.position;

        movementComponent.GoTo(target.transform.position, Speed, () =>
        {
            var sequence = DOTween.Sequence();
            sequence.AppendInterval(interactionTime).AppendCallback(() =>
            {
                resourceHeld = target.FinishedGathering();
                movementComponent.GoTo(storagePosition, Speed, () =>
                {
                    gatheringFinished?.Invoke();
                    currentState = State.Idle;
                });
            }).PlayForward();
        });
    }

    public void GiveResource(ResourceType resourceType)
    {
        resourceHeld = resourceType;
    }

    public ResourceType RemoveResource()
    {
        var temp = resourceHeld;
        resourceHeld = ResourceType.None;
        return temp;
    }
}

public enum State
{
    Idle,
    Walking,
    Gathering,
}
