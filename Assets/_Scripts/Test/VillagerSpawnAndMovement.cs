using ExtensionMethods;
using UnityEngine;

public class VillagerSpawnAndMovement : MonoBehaviour
{
    [SerializeField] private LayerMask TerrainLayerMask;
    private new Camera camera;

    private void Awake()
    {
        camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            if (FindObjectOfType<VillagerManager>().TryRequestNewVillager(out var villager))
            {
                var startPoint = villager.transform.position;
                if (camera.RaycastFromCursor(TerrainLayerMask, 1000, out var hit))
                {
                    villager.GoTo(hit.point, () =>
                    {
                        Debug.Log("Target reached, returning");
                        villager.GoTo(startPoint, () =>
                        {
                            Debug.Log("Returned, bye bye");
                            FindObjectOfType<VillagerManager>().ReturnVillager(villager);
                        });
                    });
                }
            }
        }
    }
}
