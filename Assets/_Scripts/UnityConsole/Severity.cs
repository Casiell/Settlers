using System.Collections.Generic;
using UnityEngine;

namespace UnityConsole
{
	public enum Severity
	{
		Info,
		Log,
		Warning,
		Error
	}

	public static class SeverityExtensions
	{
		private static readonly Dictionary<Severity, Color> ColorDict = new Dictionary<Severity, Color>
		{
			{ Severity.Info, Color.green }, { Severity.Log, Color.black }, { Severity.Warning, Color.yellow }, { Severity.Error, Color.red },
		};

		public static Color ToColor(this Severity severity)
		{
			return ColorDict.TryGetValue(severity, out var color) ? color : Color.magenta;
		}
	}
}