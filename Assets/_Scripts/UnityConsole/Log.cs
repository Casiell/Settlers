using System;

namespace UnityConsole
{
	public readonly struct Log
	{
		private readonly string Content;
		public readonly string Category;
		public readonly Severity Severity;
		public readonly DateTime AppearanceTime;
		private readonly string fullText;

		public string GetFullText()
		{
			return fullText;
		}

		public Log(string content, string category = "", Severity severity = Severity.Log)
		{
			this.Content = content.Replace("\n", "; ").Replace("\r", "");
			this.Category = string.IsNullOrWhiteSpace(category) ? "General" : category;
			this.Severity = severity;
			AppearanceTime = DateTime.Now;
			fullText = $"{AppearanceTime:HH:mm:ss} - {Content}";
		}
	}
}