﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace UnityConsole
{
	public class Console : MonoBehaviour
	{
		private const int BaseHeight = 1080;
		private const int BaseWidth = 1920;
		private static float multiplier = 1;
		private static int multiplierInt = 1;
		private bool isEnabled = true;
		private const string AllLetters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
		private int fontSize = GetFontSize;
		private static int GetFontSize => 20 * multiplierInt; 
		private const float ConsoleHeightPercent = .2f;

		private static float consoleStartY = GetConsoleStartY;
		private static float GetConsoleStartY => Screen.height * (1 - ConsoleHeightPercent);
		private static float consoleHeight = GetConsoleHeight;
		private static float GetConsoleHeight => Screen.height * ConsoleHeightPercent;

		private void Awake()
		{
#if !DEVELOPMENT_BUILD && !UNITY_EDITOR
			Destroy(this);
			return;
#endif
			DontDestroyOnLoad(gameObject);
		}

		private IEnumerator Start()
		{
			yield return StartCoroutine(InitTextSize());
			
			ConsoleLog.FiltersUpdated += OnFiltersUpdated;
			ConsoleLog.LogAdded += OnLogAdded;
		}

		private IEnumerator InitTextSize()
		{
			logTextStyle.fontSize = fontSize;

			yield return null;

			logVerticalLineSize = logTextStyle.CalcSize(new GUIContent(AllLetters)).y;
			logPositionIncrement = new Vector2(0, logVerticalLineSize);

			CalculateMaxWidth();
		}

		private int previousHeight = 1080;
		private int previousWidth = 1920;
		private const int CheckInterval = 30;
		private int intervalCounter = CheckInterval;
		
		private void Update()
		{
			if (++intervalCounter >= CheckInterval)
			{
				intervalCounter = 0;
				if (Screen.height != previousHeight || Screen.width != previousWidth)
				{
					Debug.Log($"{nameof(Console)}::{nameof(Update)} - Resolution changed!");
					previousHeight = Screen.height;
					previousWidth = Screen.width;
					multiplier = (float)previousHeight / BaseHeight;
					multiplierInt = previousHeight / BaseHeight;
					OnResolutionChanged(previousWidth, previousHeight);
				}
			}
		}

		private void OnResolutionChanged(int width, int height)
		{
			fontSize = GetFontSize;
			consoleStartY = GetConsoleStartY;
			consoleHeight = GetConsoleHeight;
			
			StartX = GetStartX;

			categoryScrollViewArea = GetCategoryScrollViewArea;
			logScrollViewArea = GetLogScrollViewArea;

			StartCoroutine(InitTextSize());

			stylesInitialized = false;
		}

		private void CalculateMaxWidth()
		{
			if (!ConsoleLog.FilteredLogs.Any())
			{
				maxLogWidth = 0;
				return;
			}
			maxLogWidth = ConsoleLog.FilteredLogs.Max(x => logTextStyle.CalcSize(new GUIContent(x.GetFullText())).x);
		}

		private void OnDestroy()
		{
			ConsoleLog.FiltersUpdated -= OnFiltersUpdated;
			ConsoleLog.LogAdded -= OnLogAdded;
		}

		private void OnFiltersUpdated()
		{
			CalculateMaxWidth();
		}

		private void OnLogAdded(Log log)
		{
			var width = logTextStyle.CalcSize(new GUIContent(log.GetFullText())).x;
			maxLogWidth = Mathf.Max(width, maxLogWidth);
		}

		private bool stylesInitialized = false;
		private GUIStyle skinHorizontalScrollbar;
		private GUIStyle skinVerticalScrollbar;

		private IEnumerator InitializeStyles()
		{
			categoryToggleStyle = GUI.skin.toggle;
			categoryToggleStyle.fontSize = fontSize;
			
			buttonStyle = GUI.skin.button;
			buttonStyle.fontSize = fontSize;
			toggleStyle = GUI.skin.toggle;
			toggleStyle.fontSize = fontSize;
			
			skinHorizontalScrollbar = GUI.skin.horizontalScrollbar;
			skinVerticalScrollbar = GUI.skin.verticalScrollbar;

			yield return null;
			
			snapToBottomButtonSize = toggleStyle.CalcSize(new GUIContent(SnapToBottomButtonText)) * multiplier;
			categoryLineSize = new Vector2(Screen.width * .2f, categoryToggleStyle.CalcSize(new GUIContent(AllLetters)).y);
			categoryLineVerticalSize = new Vector2(0, categoryLineSize.y);

			stylesInitialized = true;
		}

		private void OnGUI()
		{
			if (!stylesInitialized)
			{
				StartCoroutine(InitializeStyles());
			}
			
			DrawToggleButton();

			if (!isEnabled)
			{
				return;
			}

			GUI.Box(new Rect(new Vector2(0, consoleStartY), new Vector2(Screen.width * .9f, consoleHeight)), "");

			DrawCategories();
			DrawLogs();
			DrawOptions();
		}

		private GUIStyle buttonStyle;
		private GUIStyle toggleStyle;

		private void DrawToggleButton()
		{
			var buttonText = isEnabled ? "Hide Console" : "Show Console";
			var buttonSize = buttonStyle.CalcSize(new GUIContent(buttonText));
			var startX = Screen.width - buttonSize.x;
			var startY = Screen.height - buttonSize.y;
			var optionsRect = new Rect(new Vector2(startX, startY), buttonSize);

			if (GUI.Button(optionsRect, buttonText, buttonStyle))
			{
				isEnabled = !isEnabled;
			}
		}

		private bool snapToBottom = true;
		
		private const string DeselectallButtonText = "DeselectAll";
		private const string SelectAllButtonText = "SelectAll";
		private const string ClearButtonText = "Clear";
		private const string SnapToBottomButtonText = "Snap to bottom";
		private Vector2 snapToBottomButtonSize;

		private void DrawOptions()
		{
			var buttonSize = buttonStyle.CalcSize(new GUIContent(AllLetters));

			var optionsRect = new Rect(new Vector2(0, consoleStartY - buttonSize.y), Vector2.zero);

			if (DrawButton(ClearButtonText, buttonStyle, ref optionsRect))
			{
				ConsoleLog.Clear();
				logScrollPosition = Vector2.zero;
				categoriesScrollPosition = Vector2.zero;
			}

			if (DrawButton(SelectAllButtonText, buttonStyle, ref optionsRect))
			{
				ConsoleLog.ToggleCategories(ConsoleLog.GetAllCategoryNames(), true);
			}

			if (DrawButton(DeselectallButtonText, buttonStyle, ref optionsRect))
			{
				ConsoleLog.ToggleCategories(ConsoleLog.GetAllCategoryNames(), false);
			}

			optionsRect.size = snapToBottomButtonSize;
			snapToBottom = GUI.Toggle(optionsRect, snapToBottom, SnapToBottomButtonText, toggleStyle);
			optionsRect.position += new Vector2(snapToBottomButtonSize.x, 0);
		}

		private static bool DrawButton(string text, GUIStyle style, ref Rect position)
		{
			var buttonSize = style.CalcSize(new GUIContent(text));
			position.size = buttonSize;
			var ret = GUI.Button(position, text, style);

			position.position += new Vector2(buttonSize.x, 0);

			return ret;
		}

		private Vector2 categoriesScrollPosition = Vector2.one;
		private GUIStyle categoryToggleStyle = null;
		private Vector2 categoryLineSize;
		private Vector2 categoryLineVerticalSize;
		private Rect categoryScrollViewArea = GetCategoryScrollViewArea;
		private static Rect GetCategoryScrollViewArea => new Rect(new Vector2(0, consoleStartY), new Vector2(Screen.width * .2f, consoleHeight));
		private Rect categoryContentArea = Rect.zero;

		private void DrawCategories()
		{
			var categories = ConsoleLog.GetAllCategoryNames().ToList();

			if (!categories.Any())
			{
				return;
			}

			var currentRect = new Rect(Vector2.zero, categoryLineSize);

			var maxWidth = categories.Max(x => categoryToggleStyle.CalcSize(new GUIContent(x)).x);

			categoryContentArea.size = new Vector2(maxWidth, categoryLineSize.y * categories.Count);

			categoriesScrollPosition = GUI.BeginScrollView(categoryScrollViewArea, categoriesScrollPosition, categoryContentArea, skinHorizontalScrollbar, skinVerticalScrollbar);

			for (var i = 0; i < categories.Count; i++)
			{
				var category = categories[i];
				var isCategoryEnabled = GUI.Toggle(currentRect, ConsoleLog.IsCategoryActive(category), category, categoryToggleStyle);
				ConsoleLog.ToggleCategory(category, isCategoryEnabled);

				MoveRect(ref currentRect, categoryLineVerticalSize);
			}

			GUI.EndScrollView();
		}

		private Vector2 logScrollPosition = Vector2.one;
		private readonly GUIStyle logTextStyle = new GUIStyle();
		private float maxLogWidth = 0;
		private static float StartX = GetStartX;
		private static float GetStartX => Screen.width * .25f;
		private Rect logScrollViewArea = GetLogScrollViewArea;
		private static Rect GetLogScrollViewArea => new Rect(new Vector2(StartX, consoleStartY), new Vector2(Screen.width * .9f - StartX, consoleHeight)); 

		private Rect logContentArea = Rect.zero;
		private float logVerticalLineSize;
		private Vector2 logPositionIncrement;

		private static void GetFirstAndLastLineIndices(int totalLineCount, float verticalLineSize, float scrollPositionVertical, float scrollAreaHeight, out int firstIndex, out int lastIndex)
		{
			firstIndex = Mathf.Max(0, Mathf.FloorToInt(scrollPositionVertical / verticalLineSize));
			lastIndex = Mathf.Min(totalLineCount - 1, firstIndex + Mathf.CeilToInt(scrollAreaHeight / verticalLineSize) + 2);
		}

		private void DrawLogs()
		{
			var logArray = ConsoleLog.FilteredLogs;

			int logCount = logArray.Count;

			if (logCount <= 0)
			{
				return;
			}

			var contentHeight = logVerticalLineSize * logCount;

			float scrollViewHeight = logScrollViewArea.height;

			if (snapToBottom)
			{
				logScrollPosition.y = contentHeight - scrollViewHeight;
			}

			logContentArea.size = new Vector2(maxLogWidth, contentHeight);

			logScrollPosition = GUI.BeginScrollView(logScrollViewArea, logScrollPosition, logContentArea, skinHorizontalScrollbar, skinVerticalScrollbar);

			GetFirstAndLastLineIndices(logCount, logVerticalLineSize, logScrollPosition.y, scrollViewHeight, out var firstLine, out var lastLine);

			var startRect = new Rect(logPositionIncrement * firstLine, logPositionIncrement);

			for (int i = firstLine; i <= lastLine; i++)
			{
				var log = logArray[i];
				logTextStyle.normal.textColor = log.Severity.ToColor();
				GUI.Label(startRect, log.GetFullText(), logTextStyle);

				MoveRect(ref startRect, logPositionIncrement);
			}

			GUI.EndScrollView();
		}

		private static void MoveRect(ref Rect value, Vector2 positionIncrement)
		{
			value.position += positionIncrement;
		}

		private void OnApplicationQuit()
		{
			ConsoleLog.Clear();
		}
	}
}