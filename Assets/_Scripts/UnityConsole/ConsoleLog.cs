﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

namespace UnityConsole
{
	public static class ConsoleLog
	{
		public static event Action<Log> LogAdded;
		public static event Action FiltersUpdated;
		
		private static readonly string Path = Application.consoleLogPath + @"\..\LogFilters.txt";
		/// <summary>
		/// Dictionary with logs divided by their categories
		/// </summary>
		private static Dictionary<string, List<Log>> CategorizedLogDict { get; } = new Dictionary<string, List<Log>>();
		private static Dictionary<string, bool> Categories { get; } = new Dictionary<string, bool>();
		
		private static CategorySaveContainer saveContainer = new CategorySaveContainer();

		/// <summary>
		/// Logs belonging to currently active categories.
		/// Ordered by appearance time
		/// </summary>
		public static List<Log> FilteredLogs { get; private set; } = new List<Log>();

		static ConsoleLog()
		{
			LoadFilters();

#if LogSaves
			LogInfo(Path, "SavePath");
#endif
		}

		private static void LoadFilters()
		{
			if (!File.Exists(Path))
			{
#if LogSaves
				LogError($"Save file does not exist!", "SavePath");
#endif
				return;
			}

			string load;
			
			using (var reader = new StreamReader(Path))
			{
				load = reader.ReadToEnd();
			}
			saveContainer = JsonUtility.FromJson<CategorySaveContainer>(load);

#if LogSaves
			LogInfo("Save file loaded!", "SavePath");
			LogInfo($"Loaded filters: {load}", "SavePath");
#endif
		}

		public static IEnumerable<string> GetAllCategoryNames()
		{
			return Categories.Keys;
		}

		public static bool IsCategoryActive(string category)
		{
			return Categories.TryGetValue(category, out var active) && active;
		}

		private static void UpdateFilteredLogs()
		{
			FilteredLogs.Clear();
			FilteredLogs = CategorizedLogDict.Where(x => Categories[x.Key]).SelectMany(x => x.Value).OrderBy(x => x.AppearanceTime).ToList();
			FiltersUpdated?.Invoke();
		}

		/// <summary>
		/// Clears all logs from all categories
		/// </summary>
		public static void Clear()
		{
			CategorizedLogDict.Clear();
			Categories.Clear();
			UpdateFilteredLogs();
		}
	
		public static void Log(string logText, Severity severity, string category = "")
		{
			AddLog(new Log(logText, category, severity));
		}
	
		public static void Log(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Log));
		}
	
		public static void LogInfo(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Info));
		}
	
		public static void LogWarning(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Warning));
		}
	
		public static void LogError(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Error));
		}
	
		[Conditional("UNITY_EDITOR")]
		public static void LogEditor(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Log));
		}
	
		[Conditional("UNITY_EDITOR")]
		public static void LogInfoEditor(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Info));
		}
	
		[Conditional("UNITY_EDITOR")]
		public static void LogWarningEditor(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Warning));
		}
	
		[Conditional("UNITY_EDITOR")]
		public static void LogErrorEditor(string logText, string category = "")
		{
			AddLog(new Log(logText, category, Severity.Error));
		}

		/// <summary>
		/// Change active state of a category
		/// </summary>
		/// <param name="category"></param>
		/// <param name="value"></param>
		public static void ToggleCategory(string category, bool value)
		{
			bool previous = Categories[category];

			if (previous != value)
			{
				Categories[category] = value;
				UpdateFilteredLogs();
				SaveFilters();
			}
		}

		/// <summary>
		/// Change active states of multiple categories at once
		/// </summary>
		/// <param name="categories"></param>
		/// <param name="value"></param>
		public static void ToggleCategories(IEnumerable<string> categories, bool value)
		{
			categories = categories.ToList();
			bool changed = false;
			foreach (string category in categories)
			{
				bool changedLocal = Categories[category] != value;

				if (changedLocal)
				{
					Categories[category] = value;
					changed = true;
				}
			}

			if (changed)
			{
				UpdateFilteredLogs();
				SaveFilters();
			}
		}

		private static void SaveFilters()
		{
			if (!File.Exists(Path))
			{
				var stream = File.Create(Path);
				stream.Close();
#if LogSaves
				LogWarning("File doesn't exist. Created new save file!", "SavePath");
#endif
			}
			
			foreach (var pair in Categories)
			{
				var saved = saveContainer.SavedCategories.FirstOrDefault(x => x.category == pair.Key);

				if (saved != null)
				{
					saved.enabled = pair.Value;
				}
				else
				{
					saveContainer.SavedCategories.Add(new CategorySaveData(pair.Key, pair.Value));
				}
			}
			
			using (var writer = new StreamWriter(Path))
			{
				writer.Write(JsonUtility.ToJson(saveContainer));
			}
		}

		private static void AddLog(Log log)
		{
			if (!Categories.ContainsKey(log.Category))
			{
				var saved = saveContainer?.SavedCategories?.FirstOrDefault(x => x.category == log.Category);
				
				if (saved == null)
				{
					Categories.Add(log.Category, true);
					SaveFilters();				
				}
				else
				{
					Categories.Add(saved.category, saved.enabled);
				}
			}

			if (CategorizedLogDict.TryGetValue(log.Category, out var list))
			{
				list.Add(log);
			}
			else
			{
				CategorizedLogDict.Add(log.Category, new List<Log> { log });
			}

			if (Categories[log.Category])
			{
				FilteredLogs.Add(log);
				LogAdded?.Invoke(log);
			}
		}

		[Serializable]
		private class CategorySaveContainer
		{
			public List<CategorySaveData> SavedCategories = new List<CategorySaveData>(); 
		}
		
		[Serializable]
		private class CategorySaveData
		{
			public string category;
			public bool enabled;

			public CategorySaveData()
			{ }

			public CategorySaveData(string category, bool enabled)
			{
				this.category = category;
				this.enabled = enabled;
			}
		}
	}
}