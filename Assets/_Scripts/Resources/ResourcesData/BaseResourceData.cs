using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public abstract class BaseResourceData : SerializedScriptableObject
{
    [OdinSerialize] public GameObject Prefab { get; }
}