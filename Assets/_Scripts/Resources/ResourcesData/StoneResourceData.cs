﻿using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(menuName = "Create StoneResourceData", fileName = "StoneResourceData", order = 0)]
public class StoneResourceData : BaseResourceData
{
	[OdinSerialize] public int StonesToGather { get; } = 1;
}