﻿public enum ResourceType
{
	None,
	//Building
	Stone,
	Plank,
	
	//Basic
	Log,
	Grain,
	Cow,
	Water,
	
	//Intermediate
	Flour,
	
	//Food
	Bread,
	Meat,
}