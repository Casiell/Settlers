using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Settings/Resources", fileName = "ResourceSettings", order = 0)]
public class ResourceSettings : SerializedScriptableObject
{
    [OdinSerialize] public Dictionary<ResourceType, int> DefaultResources { get; private set; } = new();
}
