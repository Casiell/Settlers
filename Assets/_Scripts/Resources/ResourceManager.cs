using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class ResourceManager : SerializedMonoBehaviour
{
	public static ResourceManager Instance { get; private set; }
	public event Action<ResourceChangedParameters> ResourceCountChanged;

	[OdinSerialize, AssetsOnly, AssetSelector] private ResourceSettings settings;

	[OdinSerialize] private Dictionary<ResourceType, ResourceData> resourceDatas;
		
	private readonly Dictionary<ResourceType, int> resources = new();

	private void Awake()
	{
		Instance = this;
		foreach (var resourceType in EnumExtensions.GetEnumValues<ResourceType>())
		{
			settings.DefaultResources.TryGetValue(resourceType, out int startingCount);
			resources.Add(resourceType, startingCount);
		}
	}

	public int GetResourceCount(ResourceType resourceType)
	{
		return resources.TryGetValue(resourceType, out var count) ? count : 0;
	}

	public bool TryRemoveResource(ResourceType resourceType, int numberToRemove)
	{
		if (resources.TryGetValue(resourceType, out var count) && count >= numberToRemove)
		{
			ChangeResourceCount(resourceType, -numberToRemove);
			return true;
		}

		return false;
	}
	
	public bool TryRemoveResource(Dictionary<ResourceType, int> costTable)
	{
		if (!HasEnoughResources(costTable))
		{
			return false;
		}
		
		foreach ((var resourceType, int cost) in costTable)
		{
			ChangeResourceCount(resourceType, -cost);
		}

		return true;
	}

	public void AddResource(ResourceType resourceType, int numberToAdd)
	{
		if (numberToAdd < 0)
		{
			Debug.LogError($"Tried adding {numberToAdd.ToString()} {resourceType.ToString()}");
			return;
		}
		
		ChangeResourceCount(resourceType, numberToAdd);
	}

	private void ChangeResourceCount(ResourceType resourceType, int change)
	{
		if (resources.TryGetValue(resourceType, out var oldCount))
		{
			resources[resourceType] += change;
		}
		else
		{
			resources.Add(resourceType, change);
		}
		ResourceCountChanged?.Invoke(new ResourceChangedParameters(resourceType, oldCount, resources[resourceType], change));
	}

	public readonly struct ResourceChangedParameters
	{
		public readonly ResourceType Type;
		public readonly int OldCount;
		public readonly int NewCount;
		public readonly int Change;

		public ResourceChangedParameters(ResourceType type, int oldCount, int newCount, int change)
		{
			Type = type;
			NewCount = newCount;
			OldCount = oldCount;
			Change = change;
		}
	}

	public bool HasEnoughResources(Dictionary<ResourceType, int> costTable)
	{
		foreach ((var resourceType, int cost) in costTable)
		{
			if (!resources.TryGetValue(resourceType, out var resourceAvailable) || resourceAvailable < cost)
			{
				return false;
			}
		}

		return true;
	}
}