using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Resource Data", fileName = "ResourceData", order = 0)]
public class ResourceData : SerializedScriptableObject
{
    [OdinSerialize] public ResourceType Type { get; private set; }
    [OdinSerialize, AssetsOnly] public GameObject Prefab { get; private set; }
    [OdinSerialize, AssetsOnly] public Sprite Icon { get; private set; }
    [OdinSerialize] public ResourceCategory Category { get; private set; }
}

public enum ResourceCategory
{
    BuildingMaterial,
    Food,
    Weapon,
    Intermediate,
}