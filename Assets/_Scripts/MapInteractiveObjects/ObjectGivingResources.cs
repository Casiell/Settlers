using System.Collections.Generic;
using ExtensionMethods.Collections;
using ExtensionMethods.GameObjects;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public abstract class ObjectGivingResources<T> : SerializedMonoBehaviour, IObjectGivingResource where T : BaseResourceData
{
	[OdinSerialize] private Transform viewParent;
	[OdinSerialize, AssetList(Path = "_Prefabs/ResourceObjects/")] private List<T> possibleViews;

	[OdinSerialize] private GameObject highlightObject;

	private void Start()
	{
		SpawnView();
		Highlight(false);
	}

	private void SpawnView()
	{
		viewParent.DestroyAllChildren();
		Instantiate(possibleViews.Random(), viewParent);
	}

	public ResourceType ResourceGiven
	{
		get;
	}

	public float InteractionTime
	{
		get;
	}

	public void Highlight(bool enable)
	{
		if (highlightObject)
		{
			highlightObject.SetActive(enable);
		}
	}

	public abstract ResourceType FinishedGathering();

	private void OnDrawGizmos()
	{
		Gizmos.color = new Color(0, .5f, 0);
		var thisTransform = transform;
		var position = thisTransform.position;
		Gizmos.DrawLine(position, position + thisTransform.up * 10);
	}
}

public interface IObjectGivingResource
{
	[OdinSerialize] public ResourceType ResourceGiven { get; }

	[OdinSerialize] public float InteractionTime { get; }

	void Highlight(bool enable);
	ResourceType FinishedGathering();
}