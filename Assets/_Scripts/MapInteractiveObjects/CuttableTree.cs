public class CuttableTree : ObjectGivingResources<TreeResourceData>
{
	public override ResourceType FinishedGathering()
	{
		Destroy(gameObject);
		return ResourceGiven;
	}
}
